/**
 * Created by wojtek on 17.04.16.
 */
/* var http = require('http');
http.createServer(function(req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Hello world');
    }).listen(3000, '127.0.0.1');  */

var PORT = 33333;
var HOST = '127.0.0.1';
const buf5 = new Buffer('test');

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var request = require('request');
var currentTemp = 'null';
var currentHumid = 'null';
var currentTime = 'null';


app.get('/', function(req, res){
    res.sendfile('/root/nodeiot/index.html');
});


http.listen(3000, function(){
    console.log('listening on *:3000');
});

var dgram = require('dgram');
var server = dgram.createSocket('udp4');
var androidApp = dgram.createSocket('udp4');


server.on('listening', function () {
    var address = server.address();
    server.setBroadcast(true);
    console.log('UDP Server listening on ' + address.address + ":" + address.port);
});

server.on('message', function (message, remote) {
    console.log(remote.address + ':' + remote.port +' - ' + message);
    var parseJson =JSON.parse(message);
    currentTemp = parseJson['temp'];
    currentHumid = parseJson['humid'];
    currentTime = new Date();
    request('https://api.thingspeak.com/update?api_key=MZ4INO8KX41OKZY1&field1='+ currentTemp+'&field2=' + currentHumid, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log(body) // Print the google web page.
        }
    })
    var toSendMsg = 'Temp' + currentTemp.toString() +'C ' + 'Humid' + currentHumid + ' '  + currentTime.toString();
    io.emit('chat message', toSendMsg.toString());

});

androidApp.on('listening', function () {
    var address = androidApp.address();
    androidApp.setBroadcast(true);
    console.log('UDP Server listening on ' + address.address + ":" + address.port);
});

androidApp.on('message', function (message, remote) {
    console.log(remote.address + ':' + remote.port +' - ' + message);
    const buf5 = new Buffer('Temp ' + currentTemp.toString() +'C ' + 'Humid ' + currentHumid + ' '  + currentTime.toString());
    console.log('current temp', currentTemp);
    console.log('current humid', currentHumid);
    server.send(buf5, 0, buf5.length, remote.port, remote.address);

});

io.on('connection', function(socket){
    var toSendMsg = 'Temp' + currentTemp.toString() +'C ' + 'Humid' + currentHumid + ' '  + currentTime.toString();
    io.emit('chat message', toSendMsg.toString());});
androidApp.bind(45454);
server.bind(45453);
// server listening 0.0.0.0:41234
